#include "AbstractMeshRenderer.h"

using namespace std;

AbstractMeshRenderer::AbstractMeshRenderer()
{
	m_shaderProgram = 0;
	cubeMap = false;
}

AbstractMeshRenderer::~AbstractMeshRenderer()
{
	clearUniforms();
}

//init the shader of the meshRenderer
void AbstractMeshRenderer::initShaders(char *vertName,char *fragName)
{
	m_shaderProgram = ShaderManager::getShader(vertName,fragName);
}


void AbstractMeshRenderer::setShader(GLuint shaderProgram)
{
	if(ShaderManager::shaderExists(shaderProgram))
		m_shaderProgram = shaderProgram;
}

GLuint AbstractMeshRenderer::getShader()
{
	return m_shaderProgram;
}

void AbstractMeshRenderer::isCubeMap()
{ cubeMap = true;}

void AbstractMeshRenderer::setUniform(string name, glm::mat4 value)
{
		// deleting the previous value if exists
		if(m_uniformMat4.find(name) != m_uniformMat4.end())
		{
			m_uniformMat4.erase(name);
		}
		m_uniformMat4.insert(map<string,glm::mat4>::value_type(name,value));
}

void AbstractMeshRenderer::setUniformI1(string name, GLuint value)
{
		// deleting the previous value if exists
		if(m_uniformI1.find(name) != m_uniformI1.end())
		{
			m_uniformI1.erase(name);
		}
		m_uniformI1.insert(map<string,GLuint>::value_type(name,value));
}

void AbstractMeshRenderer::setTexture(char *fileName)
{
		tex = TextureManager::getTexture(fileName);
}

void AbstractMeshRenderer::setTexId(GLuint texId)
{
		tex = texId;
}

bool AbstractMeshRenderer::uniformExists(string name)
{
	return m_uniformMat4.find(name) != m_uniformMat4.end();
}

glm::mat4 AbstractMeshRenderer::getUniform4fv(string name)
{
	if(m_uniformMat4.find(name) != m_uniformMat4.end())
		return m_uniformMat4[name];
}

unsigned int AbstractMeshRenderer::getTextureID()
{
	return tex;
}

void AbstractMeshRenderer::deleteUniform(string name)
{
	if(m_uniformMat4.find(name) != m_uniformMat4.end())
	{
		m_uniformMat4.erase(name);
	}
}

void AbstractMeshRenderer::clearUniforms()
{
	m_uniformMat4.clear();
	m_uniformI1.clear();
}