#ifndef DEMO_MAIN_H
#define DEMO_MAIN_H

#include "BaseMeshRenderer.h"
#include "AnimatedMeshRenderer.h"
#include "bass.h"


#include <ctime>
#include <SDL.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

class DemoMain
{
public :
	static DemoMain &getIstance();
	static void deleteInstance();

	void init();
	void draw();
	void update();
	void run();
	void handleEvents();
	glm::mat4 getProjection() {return m_projection;}
	glm::mat4 getView(){ return m_playMod;}
	HSAMPLE loadSample(char * filename);

private :
	static DemoMain *s_instance; //the instance of the singleton class
	SDL_GLContext m_glContext; // OpenGL context handle
    SDL_Window *m_window; // window handle
	bool m_isSetupDone;

	void setupRC();
	static void exitFatalError(char *message);
	glm::vec3 moveForward(glm::vec3 cam, GLfloat angle, GLfloat d);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);

	DemoMain();
	~DemoMain();

	float m_timeElapsed;

	//size of the window
	int m_windowWidth, m_windowHeight;

	bool running;

	HSAMPLE mainMusic;

	md2model play;
	AnimatedMeshRenderer *playRend;
	GLuint playShader;

	Mesh *ground;
	BaseMeshRenderer *groundRend;

	Mesh *sky;
	GLuint skyShader;
	GLuint skyTexId;
	BaseMeshRenderer *skyRend;

	Mesh *build;
	int numBuilding;
	float *translateX, *translateZ;
	BaseMeshRenderer *buildingRend;

	glm::vec4 lightPos;

	float step;
	GLfloat r;

	glm::vec3 playPos;
	glm::vec3 at;
	glm::vec3 up;
	glm::mat4 m_playMod;
	glm::mat4 m_skyMod;
	glm::mat4 m_view;
	glm::mat4 m_projection;
};

#endif
