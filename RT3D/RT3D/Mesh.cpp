#include "Mesh.h"

using namespace std;

Mesh::Mesh()
{
	m_vertices = NULL;
	m_index = false;
	m_numIndices = 0;
	m_numVertices = 0;
}

Mesh::~Mesh()
{
	if(m_vertices != NULL)
		delete [] m_vertices;
}

void Mesh::init(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals, const GLfloat* texcoords, const GLuint indexCount, const GLuint* indices)
{
	m_index = true;

	m_numIndices = indexCount;

	m_vertices = new float[numVerts*3];
	for(unsigned int  i = 0; i < numVerts*3; i++)
	{
		m_vertices[i] = vertices[i];
	}

	m_numVertices = numVerts;

	m_Id = rt3d::createMesh(numVerts, vertices, colours, normals, texcoords, indexCount,indices);
}

void Mesh::init(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,	const GLfloat* texcoords)
{
	m_vertices = new float[numVerts*3];
	for(unsigned int  i = 0; i < numVerts*3; i++)
	{
		m_vertices[i] = vertices[i];
	}

	m_numVertices = numVerts;	

	m_Id = rt3d::createMesh(numVerts, vertices, colours, normals, texcoords);
}

void Mesh::init(const GLuint numVerts, const GLfloat* vertices)
{
	m_vertices = new float[numVerts*3];
	for(unsigned int  i = 0; i < numVerts*3; i++)
	{
		m_vertices[i] = vertices[i];
	}

	m_numVertices = numVerts;	

	m_Id = rt3d::createMesh(numVerts, vertices);
}


void Mesh::ReadMeshfile(char* fileName)
{
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj(fileName, verts, norms, tex_coords, indices);

	init(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), indices.size(), indices.data());
}

void Mesh::draw()
{
	if(m_index)
		rt3d::drawIndexedMesh(m_Id, m_numIndices, GL_TRIANGLES);
	else
		rt3d::drawMesh(m_Id, m_numVertices, GL_TRIANGLES); 
}
		
unsigned int Mesh::getNumVertices()
{
	return m_numVertices;
}

float *Mesh::getVertices()
{
	return m_vertices;
}