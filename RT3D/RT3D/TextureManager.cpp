#include "TextureManager.h"

using namespace std;

map<char *,GLuint> TextureManager::s_textures = map<char *,GLuint>();

GLuint TextureManager::loadTexture(char *fname, GLuint *texID, bool repeat)
{
	// generate texture ID
	glGenTextures(1, texID);

	// load file - using core SDL library
	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		cout << "Error loading bitmap" << endl;
	}
	glActiveTexture(GL_TEXTURE0); 

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, *texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if(repeat)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	
	char *type;
	GLuint colours = tmpSurface->format->BytesPerPixel;
	GLuint format, internalFormat;
	if (colours == 4) // alpha
	{
		internalFormat = GL_RGBA;
		type = "Three colors and alpha";
		if (tmpSurface->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else if (colours == 3)  // no alpha
	{
		internalFormat = GL_RGB;
		type = "Three colors";
		if (tmpSurface->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	else
	{
		format = GL_DEPTH_COMPONENT;
		internalFormat = GL_DEPTH_COMPONENT;
		type = "Depth component";
	}

	glTexImage2D(GL_TEXTURE_2D,0, internalFormat, tmpSurface->w, tmpSurface->h, 0, format, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	// texture loaded, free the temporary buffer

	if(tmpSurface)
	{

		cout << "Texture " << fname << " loaded, type : " << type << endl;
	}

	SDL_FreeSurface(tmpSurface);

	return *texID;	// return value of texure ID, redundant really
}

void TextureManager::clearTextures()
{
	map<char *,GLuint>::iterator it;
	for(it = s_textures.begin(); it != s_textures.end();it++)
		glDeleteTextures(1,&it->second);
	s_textures.clear();
}

GLuint TextureManager::getTexture(char *textureFileName, bool repeat)
{
	map<char *,GLuint>::iterator it = s_textures.find(textureFileName);
	if(it != s_textures.end())
		return it->second;
	else
	{
		GLuint newTexture;
		loadTexture(textureFileName,&newTexture, repeat);
		s_textures.insert(map<char *,GLuint>::value_type(textureFileName,newTexture));
		return newTexture;
	}
}

GLuint TextureManager::loadCubeMap(const char *fname[6])
{
	GLuint texID;
	//generate textureID
	glGenTextures(1,&texID);

	//load file - using core SDL library
	SDL_Surface *tmpSurface;

	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
						GL_TEXTURE_CUBE_MAP_POSITIVE_X,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
						GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
						GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};


	for (int i = 0; i < 6; i++)
	{
		tmpSurface = SDL_LoadBMP(fname[i]);
		if(!tmpSurface)
		{
			std::cout << "Error loading bitmap" << std::endl;
		}

		glActiveTexture(GL_TEXTURE0);

		//bind texture and set parameters
		glBindTexture(GL_TEXTURE_CUBE_MAP, texID); 

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		glTexImage2D(sides[i] ,0 ,GL_RGB,tmpSurface->w, tmpSurface->h, 0,	GL_RGB, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		
		glGenerateMipmap(GL_TEXTURE_2D);

		SDL_FreeSurface(tmpSurface);
	}

	return texID;
}

bool TextureManager::textureExists(char *textureFileName)
{
	return s_textures.find(textureFileName) != s_textures.end();
}

bool TextureManager::textureExists(GLuint textureID)
{
	map<char *,GLuint>::iterator it;
	for(it = s_textures.begin(); it != s_textures.end();it++)
		if(it->second == textureID)
			return true;
	return false;
}

void TextureManager::deleteTexture(GLuint textureID)
{
	map<char *,GLuint>::iterator it;
	for(it = s_textures.begin(); it != s_textures.end();it++)
	{
		if(it->second == textureID)
		{
			glDeleteTextures(1,&it->second);
			s_textures.erase(it->first);
			break;
		}
	}
}

