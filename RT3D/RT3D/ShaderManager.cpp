#include "ShaderManager.h"
#include "rt3d.h"

#include <iostream>

using namespace std;

//setting static variables
GLuint ShaderManager::s_shaderInUse = 0;
map<GLuint,string> ShaderManager::s_existingsShaders = map<GLuint,string>();

void ShaderManager::printShaderError(GLuint shader)
{
  int maxLength = 0;
  int logLength = 0;
  GLchar *logMessage;

  // Find out how long the error message is
  if (!glIsShader(shader))
    glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
  else
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

  if (maxLength > 0) // If message has length > 0
  {
    logMessage = new GLchar[maxLength];
    if (!glIsShader(shader))
       glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
    else
       glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
    cout << "Shader Info Log:" << endl << logMessage << endl;
    delete [] logMessage;
  }
}

void ShaderManager::clearShaders()
{
	glUseProgram(0);
	//deleting all shaders
	map<GLuint,string>::iterator it;
	for(it = s_existingsShaders.begin(); it != s_existingsShaders.end(); it++)
	{
		glDeleteProgram(it->first);
	}
	//clearing the map
	s_existingsShaders.clear();
	s_shaderInUse = 0;
}

GLuint ShaderManager::getShader(char *vertName,char *fragName)
{
	stringstream streamToFind;
	streamToFind << vertName << fragName;
	string shaderConcatenedName;
	streamToFind >> shaderConcatenedName;
	if(shaderExists(shaderConcatenedName))
	{
		map<GLuint,string>::iterator it;
		for(it = s_existingsShaders.begin(); it != s_existingsShaders.end(); it++)
			if(it->second == shaderConcatenedName)
				return it->first;
	}
	else
	{
		GLuint newShader = rt3d::initShaders(vertName,fragName);
		s_existingsShaders.insert(map<GLuint,string>::value_type(newShader,shaderConcatenedName));
		return newShader;
	}
}

bool ShaderManager::shaderExists(char *vertName,char *fragName)
{
	stringstream streamToFind;
	streamToFind << vertName << fragName;
	map<GLuint,string>::iterator it;
	for(it = s_existingsShaders.begin(); it != s_existingsShaders.end(); it++)
		if(it->second == streamToFind.str())
			return true;
	return false;
}

bool ShaderManager::shaderExists(string concatenedName)
{
	map<GLuint,string>::iterator it;
	for(it = s_existingsShaders.begin(); it != s_existingsShaders.end(); it++)
		if(it->second == concatenedName)
			return true;
	return false;
}

bool ShaderManager::shaderExists(GLuint shaderID)
{
	return s_existingsShaders.find(shaderID) != s_existingsShaders.end();
}

void ShaderManager::deleteShader(GLuint shaderID)
{
	if(shaderExists(shaderID))
	{
		s_existingsShaders.erase(shaderID);
		glDeleteProgram(shaderID);
	}
}

void ShaderManager::useShader(GLuint shaderID)
{
	if(s_shaderInUse != shaderID)
	{
		if(shaderExists(shaderID))
		{
			glUseProgram(shaderID);
			s_shaderInUse = shaderID;
		}
		else
		{
			glUseProgram(0);
			s_shaderInUse = 0;
		}
	}
}
