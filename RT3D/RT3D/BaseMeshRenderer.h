#ifndef BASE_MESH_RENDERER_H
#define BASE_MESH_RENDERER_H

#include "abstractmeshrenderer.h"
#include "Mesh.h"

class BaseMeshRenderer :
	public AbstractMeshRenderer
{
public:
	BaseMeshRenderer();
	~BaseMeshRenderer();
	void draw();
	Mesh *getMesh();
	void setMesh(Mesh *mesh);

private:
	Mesh *m_mesh;
};

#endif
