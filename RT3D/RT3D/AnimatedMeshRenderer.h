#ifndef ANIM_MESH_RENDERER_H
#define ANIM_MESH_RENDERER_H

#include "abstractmeshrenderer.h"
#include "md2model.h"

class AnimatedMeshRenderer :
	public AbstractMeshRenderer
{
public:
	AnimatedMeshRenderer();
	~AnimatedMeshRenderer();
	void draw();
	GLuint getMeshVal();
	void setMeshVal(GLuint val);
	GLuint getMeshVert();
	void setMeshVert(GLuint val);

private:
	GLuint meshVal;
	GLuint meshVert;
};

#endif