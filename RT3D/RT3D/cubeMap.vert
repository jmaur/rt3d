// cubeMap.vert
// Vertex shader for cubemap for e.g. sky box, with no lights
#version 130
uniform mat4 modelview;
uniform mat4 projection;
in  vec3 in_Position;
smooth out vec3 cubeTexCoord;
void main(void) {
	// vertex into eye coordinates
	gl_Position = projection * modelview * vec4(in_Position,1.0);
	cubeTexCoord = normalize(in_Position);
}
