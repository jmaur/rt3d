#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <GL\glew.h>
#include <SDL.h>
#include <iostream>
#include <map>

class TextureManager
{
public:
	static void clearTextures();
	static GLuint getTexture(char *textureFileName, bool repeat = true);
	static bool textureExists(char *textureFileName);
	static bool textureExists(GLuint textureID);
	static void deleteTexture(GLuint textureID);
	static GLuint loadCubeMap(const char *fname[6]);
private:
	static GLuint loadTexture(char *fname, GLuint *texID, bool repeat);

	static std::map<char *,GLuint> s_textures;
};

#endif
