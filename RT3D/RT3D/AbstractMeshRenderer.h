#ifndef ABSTRACT_MESH_RENDERER_H
#define ABSTRACT_MESH_RENDERER_H

#include <string>
#include <map>
#include <iostream>
#include <cmath>

#include <GL/glew.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderManager.h"
#include "TextureManager.h"

//to delete after creating shader and texture managers
#include <fstream>

class AbstractMeshRenderer
{
	public:
		AbstractMeshRenderer();
		virtual ~AbstractMeshRenderer();

		void initShaders(char *vertName,char *fragName);
		void initnumTextures(unsigned int numberOfTextures);
		void setShader(GLuint shaderProgram);
		GLuint getShader();
		void isCubeMap();

		//setters for uniforms
		void setTexture(char *fileName);
		void setTexId(GLuint texId);
		void setUniform(std::string name, glm::mat4 value);
		void setUniformI1(std::string name, GLuint value);
		//testing if unifroms exists
		bool uniformExists(std::string name);
		//getInformation on uniforms
		glm::mat4 getUniform4fv(std::string name);
		unsigned int getTextureID();
		//deleting uniforms
		void deleteUniform(std::string name);
		void clearUniforms();

		virtual void draw() = 0;

	protected :
		std::map<std::string,glm::mat4> m_uniformMat4;
		std::map<std::string,GLuint> m_uniformI1;
		GLuint tex;
		GLuint m_shaderProgram;
		bool cubeMap;
};

#endif
