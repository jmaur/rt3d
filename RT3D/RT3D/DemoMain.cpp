#include "DemoMain.h"
#include "rt3dObjLoader.h"
#include "rt3d.h"

#include <GL/glew.h>

#define DEG_TO_RAD 0.017453293

using namespace std;

DemoMain *DemoMain::s_instance;

////////////////
//STATIC MEMBERS
////////////////

rt3d::lightStruct light = {
	{0.4f, 0.4f, 0.4f, 1.0f}, // ambient
	{0.8f, 0.8f, 0.8f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{10.0f, 10.0f, 10.0f, 1.0f}  // position
};

rt3d::materialStruct playMat = {
	{0.4f, 0.4f, 1.0f, 1.0f}, // ambient
	{0.8f, 0.8f, 1.0f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	1.0f  // shininess
};

rt3d::materialStruct groundMat = {
	{0.4f, 0.4f, 0.4f, 1.0f}, // ambient
	{0.8f, 0.8f, 0.8f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	20.0f  // shininess
};

// Something went wrong - print error message and quit
void DemoMain::exitFatalError(char *message)
{
    cout << message << " " << endl;
    cout << SDL_GetError();
    SDL_Quit();
	cin.get();
    exit(1);
}

DemoMain &DemoMain::getIstance()
{
	if(s_instance == NULL)// if there is no current instance
		s_instance = new DemoMain();
	if(!s_instance->m_isSetupDone)//if the setup is not done
		s_instance->setupRC();
	return *s_instance;
}

void DemoMain::deleteInstance()
{
	if(s_instance != NULL)
	{
		delete s_instance;
		s_instance = NULL;
	}
}

/////////////////
//INSTANCE MEMBER
/////////////////

void DemoMain::setupRC()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // If you request a context not supported by your drivers,
    // no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
 
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on

    // Turn on x4 multisampling anti-aliasing (MSAA)
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); 
 
	m_windowWidth = 800;
	m_windowHeight = 600;

    // Create 800x600 window
    m_window = SDL_CreateWindow("SDL/GLM/OpenGL Demo",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
             m_windowWidth, m_windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if (!m_window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    // Create opengl context and attach to window
    m_glContext = SDL_GL_CreateContext(m_window);
    // set swap buffers to sync with monitor's vertical refresh rate
    SDL_GL_SetSwapInterval(1);

	GLenum err = glewInit();
	// Required on Windows... init GLEW to access OpenGL beyond 1.1
	// remove on other platforms
	if (GLEW_OK != err)
	{	// glewInit failed, something is seriously wrong.
		cout << "glewInit failed, aborting." << endl;
		exit (1);
	}

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

DemoMain::DemoMain()
{
	if(m_isSetupDone)
	{
		SDL_GL_DeleteContext(m_glContext);
		SDL_DestroyWindow(m_window);
		m_isSetupDone = false;
	}
	SDL_Quit();
}


DemoMain::~DemoMain()
{
	
}

HSAMPLE DemoMain::loadSample(char * filename)
{
	HSAMPLE sam;
	if (sam=BASS_SampleLoad(FALSE,filename,0,0,3,BASS_SAMPLE_LOOP))
		cout << "sample " << filename << " loaded!" << endl;
	else
	{
		cout << "Can't load sample";
		exit (0);
	}
	return sam;
}

void DemoMain::init()
{
	running = true;
	step = 0.25f;
	numBuilding = 16;

	lightPos = glm::vec4(-200.0f,10.0f,150.0f,1.0f);

	skyShader = ShaderManager::getShader("cubeMap.vert","cubeMap.frag");
	playShader = ShaderManager::getShader("phong-tex.vert","phong-tex.frag");
	
	rt3d::setLight(playShader,light);

	m_projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,200.0f);

	//Create Camera
	m_view = glm::mat4(1.0f);

	r = 0.0f;

	playPos = glm::vec3(0.0f, 1.0f, 4.0f);
	at = glm::vec3(0.0f, 1.0f, 3.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);

	m_playMod = glm::mat4(1.0f);
	m_playMod = glm::translate(m_playMod,playPos);

	//Create Player
	playRend = new AnimatedMeshRenderer();
	playRend->setMeshVal(play.ReadMD2Model("tris.MD2"));
	playRend->setMeshVert(play.getVertDataCount());
	playRend->setShader(playShader);
	playRend->setTexture("hobgoblin2.bmp");
	playRend->setUniform("modelview",m_playMod);

	play.setCurrentAnim(MD2_STAND);

	//Create Skybox
	sky = new Mesh();
	sky->ReadMeshfile("cube.obj");

	m_skyMod = glm::mat4 (1.0f);
	m_skyMod = glm::scale(m_skyMod,glm::vec3(3.0f,3.0f,3.0f));

	const char *cubeTexFiles[6] = {	"SkyBox5.bmp", "SkyBox4.bmp", "SkyBox2.bmp","SkyBox3.bmp", "SkyBox1.bmp", "SkyBox1.bmp" };
	skyTexId = TextureManager::loadCubeMap(cubeTexFiles);

	skyRend = new BaseMeshRenderer();
	skyRend->setMesh(sky);
	skyRend->setShader(skyShader);
	skyRend->setTexId(skyTexId);
	skyRend->isCubeMap();
	skyRend->setUniform("modelview",m_skyMod);
	skyRend->setUniformI1("cubeMap",skyTexId);

	//Create Buildings
	float tmpX[] = {80.0, 60.0, 40.0, 0.0,
					-40.0, -70.0, -85.0, -55.0, 
					-40.0, -80.0, -60.0, 80.0, 
					-20.0, -10.0, 10.0, 30.0};
	float tmpZ[] = {-80.0, -30.0, 0.0, 60.0, 
					85.0, 85.0, 85.0, 55.0, 
					-40.0, -80.0, -10.0, -60.0, 
					-20.0, 20.0, 10.0, 50.0};
	translateX = new float[numBuilding];
	translateZ = new float[numBuilding];

	build = new Mesh[3];
	build[0].ReadMeshfile("build1n4.obj");
	build[1].ReadMeshfile("build2.obj");
	build[2].ReadMeshfile("build3.obj");

	buildingRend = new BaseMeshRenderer[numBuilding];

	for(int i=0; i<numBuilding; i++)
	{
		translateX[i] = tmpX[i];
		translateZ[i] = tmpZ[i];
		buildingRend[i] = BaseMeshRenderer();
		
		if(i < numBuilding/4){
			buildingRend[i].setMesh(&build[0]);
			buildingRend[i].setTexture("building1.bmp");
		}
		if(i >= numBuilding/4 && i <= 2*numBuilding/4){
			buildingRend[i].setMesh(&build[1]);
			buildingRend[i].setTexture("building2.bmp");
		}
		if(i >= 2*numBuilding/4 && i < 3*numBuilding/4){
			buildingRend[i].setMesh(&build[2]);
			buildingRend[i].setTexture("building3.bmp");
		}
		if(i >= 3*numBuilding/4){
			buildingRend[i].setMesh(&build[0]);
			buildingRend[i].setTexture("building4.bmp");
		}
		
		buildingRend[i].setShader(playShader);
	}	

	//Create Ground
	ground = new Mesh();
	ground->ReadMeshfile("ground.obj");

	groundRend = new BaseMeshRenderer();
	groundRend->setMesh(ground);
	groundRend->setShader(playShader);
	groundRend->setTexture("beton.bmp");

	/* Initialize Musics */
	if (!BASS_Init(-1,44100,0,0,NULL))
		cout << "Can't initialize device";

	// Following comment is from source basstest file!
	/* Load a sample from "file" and give it a max of 3 simultaneous
	playings using playback position as override decider */
	mainMusic = loadSample( "serenity.mp3");
	HCHANNEL ch = BASS_SampleGetChannel(mainMusic,FALSE);
	BASS_ChannelSetAttributes(ch,-1,50,0);

	if (!BASS_ChannelPlay(ch,FALSE))
		BASS_Start();
}

glm::vec3 DemoMain::moveForward(glm::vec3 cam, GLfloat angle, GLfloat d) {
	return glm::vec3(cam.x + d*std::sin(angle*DEG_TO_RAD),cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));
}

glm::vec3 DemoMain::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) {
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD),pos.y, pos.z + d*std::sin(angle*DEG_TO_RAD));
}

void DemoMain::draw()
{
	glClearColor(1.0, 1.0, 1.0, 1.0); // set background colour
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	glDepthMask(GL_TRUE);

	//Set 3rd person camera
	at = moveForward(playPos,r,1.0f);
	at = glm::normalize(playPos-at);
	m_view = glm::lookAt(playPos-2.0f*at + glm::vec3(0.0f,0.7f,0.0f),playPos+ glm::vec3(0.0f,0.7f,0.0f),up);

	ShaderManager::useShader(skyShader);
	rt3d::setUniformMatrix4fv(skyShader, "projection", glm::value_ptr(m_projection));	
	glm::mat4 skyView = glm::mat4(glm::mat3(m_view));

	//Set SkyBox
	glCullFace(GL_FRONT);
	glDisable(GL_DEPTH_TEST);
	skyRend->setUniform("modelview",skyView*m_skyMod);
	skyRend->draw();
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);

	ShaderManager::useShader(playShader);
	rt3d::setUniformMatrix4fv(playShader, "projection", glm::value_ptr(m_projection));

	//Set LightPos
	glm::vec4 tmp = m_view*lightPos;
	rt3d::setLightPos(playShader,glm::value_ptr(tmp));

	//Draw Player
	glCullFace(GL_FRONT);
	play.Animate(0.1f);
	rt3d::updateMesh(playRend->getMeshVal(),RT3D_VERTEX,play.getAnimVerts(),play.getVertDataSize());
	rt3d::setMaterial(playShader,playMat);
	m_playMod = glm::rotate(m_playMod, 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
	m_playMod = glm::rotate(m_playMod, 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));
	m_playMod = glm::scale(m_playMod,glm::vec3(0.03f));
	playRend->setUniform("modelview",m_view*m_playMod);
	playRend->draw();
	glCullFace(GL_BACK);

	//Draw Ground
	rt3d::setMaterial(playShader,groundMat);
	groundRend->setUniform("modelview",m_view);
	groundRend->draw();

	//Draw Buildings
	//rt3d::setMaterial(playShader,buildingMat);
	glm::mat4 model;
	for(int i = 0; i<numBuilding; i++)
	{
		model = glm::mat4(1.0f);
		if( i == 6 || i == 7)
			model = glm::rotate(model, 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
		model = glm::translate(model, glm::vec3(translateX[i], 0.0f, translateZ[i]));
		buildingRend[i].setUniform("modelview",m_view*model);
		buildingRend[i].draw();
	}

	SDL_GL_SwapWindow(m_window); // swap buffers
}

void DemoMain::update()
{
	play.setCurrentAnim(MD2_STAND);
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	if ( keys[SDL_SCANCODE_W] )	{playPos = moveForward(playPos,r,-step); play.setCurrentAnim(MD2_RUN);}
	if ( keys[SDL_SCANCODE_S] )	{playPos = moveForward(playPos,r,step); play.setCurrentAnim(MD2_RUN);}
	if ( keys[SDL_SCANCODE_A] )	{playPos = moveRight(playPos,r,step); play.setCurrentAnim(MD2_RUN);}
	if ( keys[SDL_SCANCODE_D] )	{playPos = moveRight(playPos,r,-step); play.setCurrentAnim(MD2_RUN);}
	if ( keys[SDL_SCANCODE_Q] ) r -= 1.0f;
	if ( keys[SDL_SCANCODE_E] ) r += 1.0f;
	if ( keys[SDL_SCANCODE_ESCAPE] )
		running =  false;

	m_playMod = glm::translate(glm::mat4(1.0),playPos);
	m_playMod = glm::rotate(m_playMod,-r,glm::vec3(0.0f,1.0f,0.0f));
}

void DemoMain::handleEvents()
{
	SDL_Event sdlEvent;	// variable to detect SDL events

	while(SDL_PollEvent(&sdlEvent))
	{
		if (sdlEvent.type == SDL_QUIT)
			running = false;
	}
}

void DemoMain::run()
{
	clock_t currentTime, LastTime;
	currentTime = LastTime = clock();
	while (running)
	{
		currentTime = clock();
		m_timeElapsed = ((float)(currentTime - LastTime)/CLOCKS_PER_SEC);
		LastTime = currentTime;
		handleEvents();
		update();

		draw();
	}
}