#ifndef MESH
#define MESH

#include <GL/glew.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <SDL.h>
#include <string>
#include "rt3d.h"
#include "rt3dObjLoader.h"

#include "attributesDefines.h"

class Mesh{
	private:
		GLuint m_Id;
		bool m_index;
		unsigned int m_numIndices;
		unsigned int m_numVertices;
		float *m_vertices;

	public:
		Mesh();
		~Mesh();
		void init(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
			const GLfloat* texcoords, const GLuint indexCount, const GLuint* indices);

		void init(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
			const GLfloat* texcoords);

		void init(const GLuint numVerts, const GLfloat* vertices);
		void ReadMeshfile(char* fileName);
		void draw();

		unsigned int getNumVertices();
		float *getVertices();
};
#endif