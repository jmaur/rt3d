#include "AnimatedMeshRenderer.h"
#include "rt3d.h"

using namespace std;

AnimatedMeshRenderer::AnimatedMeshRenderer()
{
}


AnimatedMeshRenderer::~AnimatedMeshRenderer()
{
}

void AnimatedMeshRenderer::draw()
{
	if(meshVal != NULL)
	{
		//use the shaderProgram
		ShaderManager::useShader(m_shaderProgram);
		//iterate through the map of 4x4 matrices uniform
		map<string,glm::mat4>::iterator it4f;
		for(it4f = m_uniformMat4.begin(); it4f != m_uniformMat4.end(); it4f++)
		{
			rt3d::setUniformMatrix4fv(m_shaderProgram,it4f->first.c_str(),glm::value_ptr(it4f->second));
		}
		map<string,GLuint>::iterator it1i;
		for(it1i = m_uniformI1.begin(); it1i != m_uniformI1.end(); it1i++)
		{
			rt3d::setUniform1i(m_shaderProgram,it1i->first.c_str(),it1i->second);
		}

		if(cubeMap)
			glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
		else
			glBindTexture(GL_TEXTURE_2D, tex);
		//draw the mesh
		rt3d::drawMesh(meshVal,meshVert,GL_TRIANGLES);
	}
}

GLuint AnimatedMeshRenderer::getMeshVal()
{
	return meshVal;
}

void AnimatedMeshRenderer::setMeshVal(GLuint val)
{
	meshVal = val;
}

GLuint AnimatedMeshRenderer::getMeshVert()
{
	return meshVert;
}

void AnimatedMeshRenderer::setMeshVert(GLuint val)
{
	meshVert = val;
}